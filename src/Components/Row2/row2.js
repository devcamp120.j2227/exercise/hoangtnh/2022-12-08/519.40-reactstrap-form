import {Row, Col,Input, Label, Form, FormGroup} from "reactstrap"
import Avatar from "../../assets/images/avatar4.png"
function BodyRow2 () {
    return (
        <>
        <Row >
            <Col sm={8} style={{height:"100%"}}>
                <Form>
                    <FormGroup row>
                        <Col sm={3}>
                            <Label for="name">
                                Họ và tên
                            </Label>
                        </Col>
                        <Col sm={9}>
                            <Input id="name" type="name"/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={3}>
                            <Label for="birthday">
                                Ngày sinh
                            </Label>
                        </Col>
                        <Col sm={9}>
                            <Input id="birthday" type="text"/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={3}> 
                            <Label for="phone">
                                Số điện thoại
                            </Label>
                        </Col>
                        <Col sm={9}>
                            <Input id="phone" type="text"/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={3}>
                            <Label for="gender">
                                Giới tính
                            </Label>
                        </Col>
                        <Col sm={9}>
                            <Input id="gender" name="gender" type="select">
                            <option>
                                
                            </option>
                            <option>
                                Male
                            </option>
                            <option>
                                Female
                            </option>
                            </Input>
                        </Col>
                    </FormGroup>
                </Form>
            </Col>
            <Col sm={4} style={{ height:"100%"}} className="text-center">
                <img src={Avatar} alt="avatar"/>
            </Col>
        </Row>
        </>
    )
}
export default BodyRow2;