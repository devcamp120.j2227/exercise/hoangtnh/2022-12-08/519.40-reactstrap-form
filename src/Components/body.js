import BodyRow1 from "./Row1/row1";
import BodyRow2 from "./Row2/row2";
import BodyRow3 from "./Row3/row3";
import BodyRow4 from "./Row4/row4";

function Body () {
    return(
        <>
            <BodyRow1/>
            <BodyRow2/>
            <BodyRow3/>
            <BodyRow4/>
        </>
    )
}
export default Body;