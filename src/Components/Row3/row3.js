import {Row, Label, Input, Col, Form, FormGroup} from "reactstrap"

function BodyRow3 () {
    return (
        <>
        <Row>
            <Form>
                <FormGroup row>
                    <Col sm={2}>
                        <Label for="job">
                            Công việc
                        </Label>
                    </Col>
                    <Col sm={10}>
                        <Input id="job" name="text" type="textarea"/>
                    </Col>
                </FormGroup>
            </Form>
        </Row>
        </>
    )
}
export default BodyRow3;